@include('layouts.header')
    <form data-movie class="flex items-center justify-center vh-75" method="POST" action="/">
        @csrf
        <div class="relative ba pv4 ph3 measure">
            <div class="absolute top--1 left-1 lh-copy bg-white ph2 black">Share a youtube movie</div>
            <div class="flex items-center justify-start">
                <p class="black mr2 w5">Youtube URL :</p>
                <input class="lh-copy ph2 mr2 b--black w-100" type="url" name="youtube_url" value="">
            </div>
            <div class="flex items-center justify-start mt3">
                <p class="black mr2 w5">Title :</p>
                <input class="lh-copy ph2 mr2 b--black w-100" type="text" name="title" value="">
            </div>
            <div class="flex items-center justify-start mt3">
                <p class="black mr2 w5">Description :</p>
                <textarea class="lh-copy ph2 mr2 b--black w-100 bw1" type="text" name="description"></textarea>
            </div>
            <div class="flex items-center justify-start mt3">
                <p class="black mr2 w5"></p>
                <button class="lh-copy ph2 mr2 b--black w-100 black pointer shadow-1 bw1" type="submit">Share</button>
            </div>
        </div>
    </form>
@include('layouts.footer')

<script type="text/javascript">
    $('form[data-movie]').on('submit', function (){
        form = $(this);
        $.ajax({
          method: form.attr('method'),
          url: form.attr('action'),
          data: form.serialize(),
          beforeSend: function(xhr){
          },
          success: function(response, status, xhr){
              location.assign('/');
          },
          error: function(response, status, xhr){
              response = response.responseJSON;
              content = [];
              if (typeof response == 'undefined') {
                  content = [...content, 'You are offline'];
              } else if (response.errors) {
                  $.each(response.errors, function (key, val) {
                      content = [...content, val];
                  });
              } else {
                  content = [...content, response.message];
              }
              p_message({
                  title: 'Error',
                  content: content,
              });
          }
        });
        return false;
    });
</script>