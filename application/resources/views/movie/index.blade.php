@include('layouts.header')

<div class="w-80 center mt4">
    @foreach($movies as $key => $val)
        @php
            $val->url = preg_replace('#^.+?v=(.+?)$#mis', '$1', $val->youtube_url);
        @endphp
        <article class="flex items-start justify-start mt3">
            <div class="w-50 bg-light-gray">
                <iframe class="w-100 h5" src="https://www.youtube.com/embed/{{ $val->url }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="lh-copy w-100 ml3">
                <h1 class="b red f3">{{ $val->title }}</h1>
                <div class="flex items-center justify-start">
                    <b class="b mr1">Share by :</b> {{ $val->email }}
                    <span class="ml2 items-center justify-between {{ $user ? 'flex' : 'dn' }} {{ $val->is_like ? 'o-50' : '' }}">
                        @if(!$val->is_like || $val->is_like=='up')
                            <button data-movie_id="{{ $val->id }}" data-type="up" class="pa0 bg-transparent bn pointer" type="button">
                                <i class="material-icons f2">thumb_up</i>
                            </button>
                        @endif
                        @if(!$val->is_like || $val->is_like=='down')
                            <button data-movie_id="{{ $val->id }}" data-type="down" class="pa0 bg-transparent bn pointer" type="button">
                                <i class="material-icons f2">thumb_down</i>
                            </button>
                        @endif
                        <span class="f4 ml2">(
                            @if(!$val->is_like)
                                un-voted
                            @elseif($val->is_like=='up')
                                voted up
                            @elseif($val->is_like=='down')
                                voted down
                            @endif
                            )</span>
                    </span>
                </div>
                <p class="flex items-center">
                    <span class="mr3 flex items-center justify-start">
                        {{ $val->like_up }} <i class="material-icons f5 ml1">thumb_up</i>
                    </span>
                    <span class="flex items-center justify-start">
                        {{ $val->like_all - $val->like_up }} <i class="material-icons f5 ml1">thumb_down</i>
                    </span>
                </p>
                <p class="mt2 b">
                    Description:
                </p>
                <p>
                    {{ $val->description }}
                </p>
            </div>
        </article>
    @endforeach
</div>

@include('layouts.footer')

<script type="text/javascript">
    $('[data-type]').on('click', function (){
        if($(this).closest('span').hasClass('o-50')){
            p_message({
                title: 'Error',
                content: ['You have like this']
            });
            return false;
        }
        $.ajax({
          method: 'POST',
          url: '/like',
          data: {
              _token : '{{ csrf_token() }}',
              movie_id : $(this).attr('data-movie_id'),
              type : $(this).attr('data-type'),
          },
          beforeSend: function(xhr){
          },
          success: function(response, status, xhr){
              location.reload();
          },
          error: function(response, status, xhr){
              response = response.responseJSON;
              content = [];
              if (typeof response == 'undefined') {
                  content = [...content, 'You are offline'];
              } else if (response.errors) {
                  $.each(response.errors, function (key, val) {
                      content = [...content, val];
                  });
              } else {
                  content = [...content, response.message];
              }
              p_message({
                  title: 'Error',
                  content: content,
              });
          }
        });
    });
</script>
