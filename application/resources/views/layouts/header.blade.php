<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
{{--    <link rel="manifest" href="{{ asset('/manifest.json') }}">--}}

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{ asset('/css/tachyons.min.css') }}" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">

    <meta property="og:image" content="{{ asset('/images/icons/icon-512x512.png') }}">
    <link rel="shortcut icon" href="{{ asset('/images/icons/icon-512x512.png') }}">
</head>

<body class="w-80 center">
    <header class="flex items-center justify-between pv3 bb bw2 b--black">
        <a href="/" class="flex items-center no-underline b black f2">
            <i class="material-icons mr1 f2">home</i> Funny Movies
        </a>
        @if($user)
            <div>
                {{ $user->email }}
                <a href="/create" class="ml2 lh-copy bg-transparent shadow-1 pointer ba bw1 b--black no-underline pv1 ph2 black">Share a movie</a>
                <a href="/logout" onclick="document.getElementById('logout-form').submit();return false;" class="ml2 lh-copy bg-transparent shadow-1 pointer ba bw1 b--black no-underline pv1 ph2 black">Logout</a>
                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                    <?php echo csrf_field(); ?>
                </form>
            </div>
        @else
            <form data-register method="POST" action="{{ route('register') }}" class="flex items-center justify-between">
                @csrf
                <input class="lh-title w4 ph2 mr2 b--black" type="email" name="email" placeholder="email">
                <input class="lh-title w4 ph2 mr2 b--black" type="password" name="password" placeholder="password">
                <button class="lh-title bg-transparent shadow-1 pointer bw1 b--black" type="submit">Login/Register</button>
                <input name="password_confirmation" type="hidden">
                <input name="remember" type="hidden" value="on">
            </form>
        @endif
    </header>
    <main>
