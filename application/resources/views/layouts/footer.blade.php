
    </main>
</body>
<script src="{{ asset('/js/jquery-3.4.1.min.js') }}"></script>
<script type="text/javascript">
    $('form[data-register]').on('submit', function (){
        form = $(this);
        form.find('[name=password_confirmation]').val(form.find('[name=password]').val());
        $.ajax({
            method: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            beforeSend: function(xhr){
            },
            success: function(response, status, xhr){
            },
            error: function(response, status, xhr){
                response = response.responseJSON;
                if(xhr=='Not Found'){
                    location.reload();
                    return false;
                }
                if(typeof response.errors.email != 'undefined' && response.errors.email[0]=='The email has already been taken.'){
                    login(form);
                    return false;
                }
                content = [];
                if (typeof response == 'undefined') {
                    content = [...content, 'You are offline'];
                } else if (response.errors) {
                    $.each(response.errors, function (key, val) {
                        content = [...content, val];
                    });
                } else {
                    content = [...content, response.message];
                }
                p_message({
                    title: 'Error',
                    content: content,
                });
            }
        });
        return false;
    });

    function login(form){
        $.ajax({
            method: form.attr('method'),
            url: '/login',
            data: form.serialize(),
            beforeSend: function(xhr){
            },
            success: function(response, status, xhr){
            },
            error: function(response, status, xhr){
                response = response.responseJSON;
                if(xhr=='Not Found'){
                    location.reload();
                    return false;
                }
                content = [];
                if (typeof response == 'undefined') {
                    content = [...content, 'You are offline'];
                } else if (response.errors) {
                    $.each(response.errors, function (key, val) {
                        content = [...content, val];
                    });
                } else {
                    content = [...content, response.message];
                }
                p_message({
                    title: 'Error',
                    content: content,
                });
            }
        });
    }
</script>

<!-- p-message -->
<style>
    .p-modal-window {
        position: fixed;
        background-color: rgba(0, 0, 0, 0.45);
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999999;
        opacity: 0;
        pointer-events: none;
        transition: all 0.3s;
        overflow-y: auto;
    }
    /*.p-modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }*/
    .p-modal-window > div {
        width: 500px;
        position: relative;
        min-height: calc(100% - (1.75rem * 2));
        margin: 1.75rem auto;
        transform: translate(0, 0);
        display: flex;
        align-items: center;
    }
    .p-modal-window > div .p-modal-content {
        width: 100%;
        background: #ffffff;
        padding: 2em;
        border-radius: 5px;
    }

    .p-modal-window header {
        font-weight: bold;
    }

    .p-modal-window h1 {
        font-size: 1.5em;
        margin: 0 0 15px;
        position: relative;
    }
    .p-modal-close {
        color: #aaa;
        position: absolute;
        top: -0.3em;
        right: 0;
        font-size: 1.5em;
    }
    .p-modal-close:hover {
        color: black;
        text-decoration: none;
    }
    /* p-message */
    .p-modal-window > div{
        width: 400px;
    }
    @media screen and (max-width: 30em){
        .p-modal-window > div{
            width: 80%;
        }
    }
    .p-modal-window > div .p-message-content{
        padding: 1em 0 0;
        border-radius: .5em;
    }
    .p-message-content > h1, .p-message-content > div, .p-message-content > p a{
        text-align: center;
        line-height: 1.5em;
        display: block;
    }
    .p-message-content > h1{
        font-size: 1.2em;
        font-weight: bold;
        margin-bottom: .2em;
    }
    .p-message-content > div{
        padding: 0 1em;
    }
    .p-message-content > hr{
        border: none;
        border-bottom: solid 1px #ccc;
        margin: 0.8em 0 0;
    }
    .p-message-content > p{
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
    .p-message-content > p a{
        padding: 0.5em 0 0.6em;
        color: blue;
        font-weight: bold;
        text-decoration: none;
        width: 100%;
    }
    .p-message-content > p a:first-child{
        color: gray;
        border-right: #ccc solid 1px;
        font-weight: normal;
    }
</style>
<script type="text/javascript">
    function p_modal(){
        body = document.getElementsByTagName("BODY")[0];
        let modal = document.querySelectorAll('.p-modal-window')[0];
        if(typeof modal != 'undefined' && modal.style.opacity=='1' || window.location.href.match(/\#p-open-modal/gim)){
            body.style.overflow = 'hidden';
            body.style.maxHeight = '100vh';
        }else{
            body.style.overflow = 'auto';
            body.style.maxHeight = 'auto';
        }
    }
    var p_message_data = localStorage.getItem('p_message_data');
    p_message_data = p_message_data === null ? {} : JSON.parse(p_message_data);
    function detect_message(){
        if(window.location.href.match(/\#p-open-modal/gim)){
            p_message(p_message_data);
        }
        //p_message({type:'close'});
    }
    /*window.addEventListener('hashchange', function(e){
        detect_message();
    });*/
    $(function (){
        detect_message();
    });
    var p_message_init = {
        title : 'Error',
        content : ['Connect fail'],
        button : ['Cancel', 'OK'],
    };

    function p_message(data){
        return new Promise((resolve, reject) => {
            var {title, content, button, type, change_url} = data;
            localStorage.setItem('p_message_data', JSON.stringify(data));
            let modal = document.querySelectorAll('.p-message-window')[0];
            modal.style.opacity = 0;
            modal.style.pointerEvents = 'none';
            if (!type) {
                button = button && button == 2 ? p_message_init.button : ['', p_message_init.button[1]];
                modal.style.opacity = 1;
                modal.style.pointerEvents = 'auto';
                document.querySelectorAll('.p-message-content > h1')[0].innerHTML = title ? title : p_message_init.title;
                content = content.join('<br/>');
                document.querySelectorAll('.p-message-content > div')[0].innerHTML = content ? content : p_message_init.content;
                if (button[0] != '') {
                    document.querySelectorAll('.p-message-content > p a')[0].innerHTML = button[0];
                    document.querySelectorAll('.p-message-content > p a')[0].style.display = 'block';
                } else {
                    document.querySelectorAll('.p-message-content > p a')[0].style.display = 'none';
                }
                document.querySelectorAll('.p-message-content > p a')[1].innerHTML = button[1];
                if(change_url)
                    parent.location.hash = "p-open-modal";
                $('[href*=p-ok-modal]').on('click', function (){
                    resolve();
                });
                $('[href*=p-close-modal]').on('click', function (){
                    reject();
                });
            } else if(change_url) {
                parent.location.hash = "p-close-modal";
            }
            p_modal();
        });
    }
</script>
<div id="p-open-modal" class="p-modal-window p-message-window">
    <div>
        <div class="p-modal-content p-message-content">
            <h1></h1>
            <div></div>
            <hr>
            <p>
                <a href="#p-close-modal" onclick="return p_message({type:'close'})"></a>
                <a href="#p-ok-modal" onclick="return p_message({type:'close'});"></a>
            </p>
        </div>
    </div>
</div>
<!-- /p-message -->