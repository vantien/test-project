<?php

namespace App\Http\Controllers;

use App\Movie;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = session()->get('msg_user') ? session()->get('msg_user')->id : 0;
        $data = [
            'user' => User::where('id', $user_id)->first(),
            'movies' => DB::table('movies')
                ->select('movies.*', DB::raw('email, COUNT(likes.id) AS like_all, SUM(CASE WHEN type="up" THEN 1 ELSE 0 END) AS like_up'))
                ->join('users', 'users.id', '=', 'movies.user_id')
                ->leftJoin('likes', 'likes.movie_id', '=', 'movies.id')
                ->groupBy('movies.id')
                ->orderBy('movies.id', 'DESC')
                ->get(),
            'likes' => DB::table('likes')
                ->select('movie_id', 'type')
                ->where('user_id', '=', $user_id)
                ->get(),
        ];
        foreach ($data['movies'] as $key => $val) {
            $val->is_like = false;
            foreach ($data['likes'] as $key1 => $val1){
                if($val->id==$val1->movie_id)
                    $val->is_like = $val1->type;
            }
            $data['movies'][$key] = $val;
        }
        return view('movie.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Session::get('msg_user'))
            return redirect('/');
        $data = [
            'user' => User::where('id', session()->get('msg_user')->id)->first(),
        ];
        return view('movie.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'youtube_url' => [
                'unique:movies',
                'required',
                'max:255',
                'regex:/(?:(?:http|https):\/\/)?(?:www.)?youtube.com\/watch\?v=/u'
            ],
            'title' => 'required',
        ]);
        $data = $request->except(['_token']);
        $data['user_id'] = session()->get('msg_user')->id;
        if($movie = Movie::create($data)){
            return response()->json($movie);
        }
        return response()->json(['errors'=>['error'=>'Can\'t save']], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        //
    }
}
