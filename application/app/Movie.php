<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = [
        'user_id',
        'youtube_url',
        'title',
        'description',
    ];
}
