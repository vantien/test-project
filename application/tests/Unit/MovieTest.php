<?php

namespace Tests\Unit;

use App\Movie;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MovieTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    use DatabaseTransactions;

    /*public function test_list_movie()
    {
        parent::AuthLogin();
        $this->get('/')
            ->assertViewIs('movie.index');
    }*/

    public function test_share_movie()
    {
        parent::AuthLogin();

        $movie_fake = factory(Movie::class)->make()->toArray();
        $movie_fake['_token'] = '32432424324';//csrf_token();
        $movie_fake['youtube_url'] = 'https://www.youtube.com/watch?v=aTirRceIIG8dd';

        $response = $this->call('POST', '/', $movie_fake);
//            ->assertSessionHasErrors(['youtube_url']);
        dd($response->getContent());
    }
}
