<?php

namespace Tests;

use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function AuthLogin(){
        $user_fake = factory(User::class)->make();
        $user = new User();
        $user->password = $user_fake->password;
        $user->email = $user_fake->email;
        $user->save();
        session(['msg_user' => $user]);
    }
}
